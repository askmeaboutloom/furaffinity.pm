use Test::Most tests => 2;
use Mojo::DOM;
use File::Slurp qw(slurp);
use FurAffinity;

sub html
{
    my ($file) = @_;
    Mojo::DOM->new(scalar slurp("html/$file.html"))
}

my %no_messages = map { $_ => 0 } qw(total submissions journals comments
                                     favorites watches notes tickets);


my $fa = FurAffinity->new('someuser');

is_deeply $fa->fetch_messages(html('no_messages')), \%no_messages,
             'fetch no messages';

is_deeply $fa->fetch_messages(html('messages')), {
              %no_messages,
              comments  => 23,
              favorites => 61,
              watches   => 124,
              total     => 208,
          }, 'fetch messages';
