package FurAffinity;
$VERSION = '0.02_01';
$VERSION = eval $VERSION;
use strict;
use warnings;
use Log::Message::Simple;
use Mojo::DOM;
use WWW::Mechanize;
use Exception::Class (
    Except        => {
        description => 'Base exception class for the FurAffinity module',
    },
    ArgExcept     => {
        isa         => 'Except',
        description => 'Incorrect or missing argument',
    },
    RequestExcept => {
        isa         => 'Except',
        description => 'Request failed',
        fields      => ['method'],
    },
    PageExcept    => {
        isa         => 'Except',
        description => "A page didn't look as expected",
    },
    FormExcept    => {
        isa         => 'Except',
        description => 'A form field was invalidated on the client side',
        fields      => ['field', 'check'],
    },
);

our $fa      = 'https://www.furaffinity.net';
our $charset = 'utf-8';
our $top     = 'table.block-menu-top > tr > td.header_bkg ';

our ($msg, $debug, %criteria);


sub new {
    my ($class, $name) = @_;
    Except->throw(error => 'Missing name in constructor') if !defined $name;
    Except->throw(error => 'Empty name in constructor'  ) if $name !~ /\S/;
    bless {
        name      => $name,
        mech      => WWW::Mechanize->new,
        logged_in => 0,
    } => $class;
}

sub DESTROY {
    my ($self) = @_;
    $self->log_out if $self->logged_in;
}

sub name      { $_[0]->{name} }
sub mech      { $_[0]->{mech} }
sub logged_in {
    my ($self, $arg) = @_;
    $self->{logged_in} = $arg if defined $arg;
    return $self->{logged_in};
}


sub dom { Mojo::DOM->new($_[0]->mech->content(charset => $charset)) }

sub get {
    my $self = shift;
    my $mech = $self->mech;
    debug "get @_", $debug;
    $mech->get(@_);
    return $self->dom if $mech->success;
    RequestExcept->throw(method => 'GET', error => $mech->res->status_line);
};

sub post {
    my $self = shift;
    my $mech = $self->mech;
    debug "post @_", $debug;
    $mech->post(@_);
    return $self->dom if $mech->success;
    RequestExcept->throw(method => 'POST', error => $mech->res->status_line);
};


sub log_in {
    my ($self, $pass) = @_;
    msg 'Logging in ' . $self->name, $msg;
    $self->get("$fa/login/");
    eval {
        $self->mech->submit_form(
            fields => {name => $self->name, pass => $pass}
        );
    };
    PageExcept->throw(error => $@) if $@;
    return $self;
}

sub log_out {
    my ($self) = @_;
    msg 'Logging out ' . $self->name, $msg;
    $self->get("$fa/logout/");
    return $self;
}

sub check_logged_in {
    my ($self, $dom) = @_;
    msg 'Checking login status of ' . $self->name, $msg;
    $dom //= $self->get($fa);

    return $self->logged_in(1) if defined $self->scrape_user($dom);
    return $self->logged_in(0) if $dom->at("$top> .dropdown-left > .noblock");

    PageExcept->throw(error => "Can't tell if " . $self->name
                             . ' is logged in or not.');
}


our %field_checkers = (
    max      => sub { length $_[0] <= $_[1]        },
    min      => sub { length $_[0] >= $_[1]        },
    regex    => sub { $_[0] =~ $_[1]               },
    inhash   => sub { exists $_[1]->{$_[0]}        },
    exists   => sub { $_[1] ? -e $_[0] : !-e $_[0] },
    optional => sub { 1                            },
);

sub check_fields {
    my ($fields, $restrictions) = @_;
    while (my ($name, $restrict) = each %$restrictions) {
        my $field = $fields->{$name};
        next if $restrict->{optional} && !defined $field;
        while (my ($k, $v) = each %$restrict) {
            if (!$field_checkers{$k}->($field, $v)) {
                FormExcept->throw(
                    error => "$name does not pass $k => $v with $field",
                    field => [$name => $field],
                    check => [$k    => $v    ],
                );
            }
        }
    }
}


sub scrape_user {
    my ($self, $dom) = @_;
    my $sel = $dom->at('#my-username');
    return $sel->text . '' if $sel;
    return undef;
}

sub scrape_messages {
    my ($self, $dom) = @_;

    # No messages
    if ($dom->at("$top> .dropdown-left > .noblock > a:not([href]) > b")) {
        return {map { $_ => 0 } qw(total submissions journals comments
                                   favorites watches notes tickets)};
    }

    my $sel   = $dom->at("$top> .dropdown-left > .noblock");
    my $sel_s = $sel->at('a[href="/msg/submissions/"]'  );
    my $sel_o = $sel->at('a[href="/msg/others/"]'       );
    my $sel_n = $sel->at('a[href="/msg/pms/"]'          );
    my $sel_t = $sel->at('a[href="/msg/troubletickets"]');
    return undef unless $sel && $sel_s && $sel_o && $sel_n && $sel_t;

    my $total = 0;
    my %messages;
    # Match number of each message type and on success put it into %messages.
    # On failure (no messages of that type), put 0 into it. Whatever was put
    # into the hash will be added to the $total amount of messages.
    $total += $messages{submissions} = $sel_s->text =~ /(\d+)/  ? int $1 : 0;
    $total += $messages{comments   } = $sel_o->text =~ /(\d+)C/ ? int $1 : 0;
    $total += $messages{journals   } = $sel_o->text =~ /(\d+)J/ ? int $1 : 0;
    $total += $messages{favorites  } = $sel_o->text =~ /(\d+)F/ ? int $1 : 0;
    $total += $messages{watches    } = $sel_o->text =~ /(\d+)W/ ? int $1 : 0;
    $total += $messages{notes      } = $sel_n->text =~ /(\d+)/  ? int $1 : 0;
    # TODO: Does this work? I never had any trouble tickets.
    $total += $messages{tickets    } = $sel_t->text =~ /(\d+)/  ? int $1 : 0;
              $messages{total      } = $total;

    return \%messages;
}

sub scrape_submissions {
    my ($self, $dom) = @_;
    my @submissions;

    for my $type (qw(image text audio flash)) { # are there more types maybe?
        for my $sel ($dom->at(".t-$type")->each) {
            my $id     = $sel->at('u > s > a')->attr('href')
                                                 =~ m{^/view/(.+)/$} && $1;
            my $artist = $sel->at('small > a')->attr('href')
                                                 =~ m{^/user/(.+)/$} && $1;
            my $thumb  = 'https:' . $sel->at('img')->attr('src');
            $thumb =~ s/@\d+?-/\@100-/;
            push @submissions, {
                type   => $type,
                id     => $id,
                artist => $artist,
                title  => $sel->at('span')->text . '',
                thumb  => $thumb,
            };
        }
    }

    return \@submissions;
}

sub scrape_submission {
    my ($self, $dom) = @_;
    my %submission;

    # the page title has both the submission title and the artist
    my $title = $dom->at('title')->first->text;
    if ($title =~ /^\s*(.*)\s*by\s*(.*?)\s*-- Fur Affinity \[dot\] net\s*$/) {
        $submission{title } = $1;
        $submission{artist} = $2;
    }

    for my $sel ($dom->at('.alt1.actions b > a')->each) {
        my $href = $sel->attr('href');

        if      ($href =~ m{^//d\.facdn\.net/(.*)$}) {
           $submission{file      } = "https://d.facdn.net/$1";

        } elsif ($href =~ m{^/full/(\d+)/$}
              || $href =~ m{^/view/(\d+)/$}) {
            $submission{id        } = $1;

        } elsif ($href =~ m{^/gallery/(.*)/$}) {
            $submission{artist_url} = $1;
        }
    }

    my $desc = $dom->at('td[valign="top"][align="left"]'
                        . '[width="70%"][class="alt1"]')->content;
    $submission{description} = "$desc";

    my $date = $dom->at('td[valign="top"][align="left"][class="alt1"]'
                        . '> .popup_date');
    # Depending on the user's setting, the absolute date is in the text and the
    # fuzzy date is in the title or vice-versa. We want the absolute one, so we
    # match against something like "August 15th, 2014 12:34 PM".
    # regex breakdown:  month    day  th,     year   hour:minute   AM/PM
    if ($date->text =~ / \S+ \s+ \d+ \S+, \s+ \d+ \s+ \d+:\d+   \s+ \S+ /x) {
        $submission{date} = $date->text . '';
    } else {
        $submission{date} = $date->attr('title') . '';
    }

    $submission{keywords} = [];
    for my $sel ($dom->at('#keywords > a')->each) {
        push @{$submission{keywords}}, $sel->text;
    }

    # TODO comments
    # TODO maybe other submission info, but eh, scrape_info already does that

    return \%submission;
}

our %id_to_rating = qw(0 general 1 adult 2 mature);

sub scrape_info {
    my ($self, $dom) = @_;
    my  %info;

    for (['cat', 'category'], ['atype', 'theme'], 'species', 'gender') {
        my ($name, $key) = ref $_ ? @$_ : ($_, $_);
        my $text = $dom->at(qq(select[name="$name"] option[selected]))->text;
        $info{$key} = "$text";
    }

    $info{scraps} = $dom->at('input[name="scrap"][checked]')->children->size;

    my $rid = $dom->at('input[name="rating"][checked]')->val;
    $info{rating} = $id_to_rating{$rid};

    my $title = $dom->at('input[name="title"]')->val;
    $info{title} = "$title";

    my $description = $dom->at('textarea[name="message"]')->text;
    $info{description} = "$description";

    my @keywords = split /\s+/, $dom->at('textarea[name="keywords"]')->text;
    $info{keywords} = \@keywords;

    return \%info;
}

our %scrapers = (
    user        => 'scrape_user',
    messages    => 'scrape_messages',
    submissions => 'scrape_submissions',
    submission  => 'scrape_submission',
    info        => 'scrape_info',
);

sub scrape {
    my $self = shift;
    my %args = ref $_[0] eq 'HASH' ? %{$_[0]} : @_;

    my $list = ref $args{scrape} eq 'ARRAY' && $args{scrape}
        or ArgExcept->throw(error => 'Missing list of scrapers');

    my $pg  = $args{page};
    my $dom = defined $pg ? ref $pg ? $pg : $self->get($pg) : $self->dom;

    my %result;
    for my $key (@$list) {
        my $scraper = $scrapers{$key}
            or ArgExcept->throw(error => "``$key'' is not a scraper");
        $result{$key} = $self->$scraper($dom);
    }
    return \%result;
}


sub fetch_messages {
    my ($self, $page) = @_;
    return $self->scrape(
        page   => $page // $self->get($fa),
        scrape => ['messages'],
    )->{messages};
}

sub fetch_submissions {
    my $self = shift;
    my %args = (
        from   => undef,
        number => 1,
        user   => $self->name,
        ref $_[0] eq 'HASH' ? %{$_[0]} : @_,
    );

    $args{from} =~ /^(gallery|scraps|favorites)$/
        or ArgExcept->throw(error => "Can't get submissions from ``$args{from}"
                                   . "''. Valid values are ``gallery'', "
                                   . "``scraps'' and ``favorites''.");

    return $self->scrape(
        page   => "$fa/$args{from}/$args{user}/$args{number}/",
        scrape => ['submissions'],
    )->{submissions};
}

sub fetch_submission {
    my $self = shift;
    my %args = ref $_[0] eq 'HASH' ? %{$_[0]} : @_;

    $args{id}=~ /^\d+$/
        or ArgExcept->throw(error => "ID must be an unsigned number.");

    my $submission = $self->scrape(
        page   => "$fa/view/$args{id}/",
        scrape => [qw(submission)],
    );

    if ($submission->{submission}{artist} eq $self->name) {
        return {%$submission, %{$self->scrape(
            page   => "$fa/controls/submissions/changeinfo/$args{id}/",
            scrape => [qw(info)],
        )}};
    } else {
        return $submission;
    }
}


sub change_info {
    my $self = shift;
    my $mech = $self->mech;
    my %fields = ref $_[0] eq 'HASH' ? %{$_[0]} : @_;

    check_fields(\%fields, {
        id          => {regex => qw/^\d+$/              },
        title       => {regex  => qr/\S/,  optional => 1},
        description => {regex  => qr/\S/,  optional => 1},
        keywords    => {max    => 250,     optional => 1},
        # TODO other fields
    });

    my %form;
    $form{title   } = $fields{title      } if exists $fields{title      };
    $form{message } = $fields{description} if exists $fields{description};
    $form{keywords} = $fields{keywords   } if exists $fields{keywords   };

    $self->get("$fa/controls/submissions/changeinfo/$fields{id}/")
        unless $fields{no_load};
    eval {
        $mech->submit_form(
            form_name => 'MsgForm',
            fields    => \%form,
        );
    };
    PageExcept->throw(error => $@) if $@;

    $mech->uri =~ m{/view/$fields{id}/} or PageExcept->throw(
        error => 'Unexpected submission result URL: ' . $mech->uri,
    );
}

sub change_submission_file {
    my $self   = shift;
    my $mech   = $self->mech;
    my %fields = (
        rebuild_thumb => 0,
        ref $_[0] eq 'HASH' ? %{$_[0]} : @_,
    );

    check_fields(\%fields, {
        id            => {regex  => qw/^\d+$/},
        file          => {exists => 1        },
    });

    $self->get("$fa/controls/submissions/changesubmission/$fields{id}/");
    eval {
        $mech->submit_form(
            form_name => 'uploadform',
            fields    => {
                'newsubmission'     => $fields{file},
                'rebuild-thumbnail' => $fields{rebuild_thumb} ? 1 : 0,
            },
        );
    };
    PageExcept->throw(error => $@) if $@;

    $mech->uri =~ m{/view/(\d+)/} or PageExcept->throw(
        error => 'Unexpected submission result URL: ' . $mech->uri,
    );
    return int $1;
}


our %rating_to_id = reverse %id_to_rating;

sub submit_artwork {
    my $self  = shift;
    my $mech  = $self->mech;
    my %fields = (
        keywords => '',
        ref $_[0] eq 'HASH' ? %{$_[0]} : @_,
    );
    for my $key (keys %criteria) {
        next if not defined $fields{$key};
        $fields{$key} =~ s/\s+//g;
        $fields{$key} =  lc $fields{$key};
    }
    msg 'Submitting artwork for ' . $self->name, $msg;

    check_fields(\%fields, {
        file        => {exists => 1                  },
        thumb       => {exists => 1, optional => 1   },
        title       => {regex  => qr/\S/             },
        description => {regex  => qr/\S/             },
        keywords    => {max    => 250                },
        rating      => {inhash => \%rating_to_id     },
        category    => {inhash => $criteria{category}},
        theme       => {inhash => $criteria{theme   }},
        species     => {inhash => $criteria{species }},
        gender      => {inhash => $criteria{gender  }},
    });

    msg 'Starting submission process...', $msg;
    $self->get("$fa/submit/");
    eval {
        $mech->submit_form(
            form_name => 'myform',
            fields    => {submission_type => 'submission'},
        );
    };
    PageExcept->throw(error => $@) if $@;

    msg 'Uploading...', $msg;
    eval {
        $mech->submit_form(
            form_name => 'myform',
            fields    => {
                submission => $fields{file},
                defined $fields{thumb} ? (thumbnail => $fields{thumb}) : (),
            },
        );
    };
    PageExcept->throw(error => $@) if $@;

    msg 'Finalizing...', $msg;
    eval {
        $mech->submit_form(
            form_name => 'myform',
            fields    => {
                title    => $fields{title},
                message  => $fields{description},
                keywords => $fields{keywords},
                rating   => $fields{rating},
                scrap    => $fields{scraps} ? 1 : 0,
                cat      => $criteria{category}{$fields{category}} // 1,
                atype    => $criteria{theme   }{$fields{theme   }} // 1,
                species  => $criteria{species }{$fields{species }} // 1,
                gender   => $criteria{gender  }{$fields{gender  }} // 1,
            },
        );
    };
    PageExcept->throw(error => $@) if $@;

    $mech->uri =~ m{/view/(\d+)/} or PageExcept->throw(
        error => 'Unexpected submission result URL: ' . $mech->uri,
    );
    return int $1;
}

sub submit_note {
    my $self   = shift;
    my %fields = (
        subject => '',
        ref $_[0] eq 'HASH' ? %{$_[0]} : @_,
    );

    check_fields(\%fields, {
        to      => {max => 150, regex => qr/\S/},
        subject => {max => 150                 },
        message => {            regex => qr/\S/},
    });

    msg 'Sending message from ' . $self->name . " to $fields{to}", $msg;
    $self->get("$fa/msg/compose/");
    eval {
        $self->mech->submit_form(
            form_name => 'MsgForm',
            fields    => \%fields,
        );
    };
    PageExcept->throw(error => $@) if $@;
    # TODO: check if that worked
    return $self;
}


### generated by scripts/extract_criteria ###
%criteria = (
    'category' => {
        'adoptables' => '21',
        'all' => '1',
        'artwork(digital)' => '2',
        'artwork(traditional)' => '3',
        'auctions' => '22',
        'cellshading' => '4',
        'contests' => '23',
        'crafting' => '5',
        'currentevents' => '24',
        'designs' => '6',
        'desktops' => '25',
        'flash' => '7',
        'fursuiting' => '8',
        'handhelds' => '19',
        'icons' => '9',
        'mosaics' => '10',
        'music' => '16',
        'other' => '31',
        'photography' => '11',
        'podcasts' => '17',
        'poetry' => '14',
        'prose' => '15',
        'resources' => '20',
        'scraps' => '28',
        'screenshots' => '27',
        'sculpting' => '12',
        'skins' => '18',
        'stockart' => '26',
        'story' => '13',
        'wallpaper' => '29',
        'ych' => '30'
    },
    'gender' => {
        'any' => '0',
        'female' => '3',
        'herm' => '4',
        'male' => '2',
        'multiplecharacters' => '6',
        'other/notspecified' => '7',
        'transgender' => '5'
    },
    'species' => {
        'alien' => '5001',
        'alligator' => '7001',
        'amphibian(other)' => '1000',
        'aquatic(other)' => '2000',
        'arachnid' => '8000',
        'argonian' => '5002',
        'avian(other)' => '3000',
        'bat' => '6001',
        'bear' => '6002',
        'bovid(other)' => '6007',
        'bovid-antelope' => '6004',
        'bovid-bovines' => '6003',
        'bovid-gazelle' => '6005',
        'bovid-goat' => '6006',
        'canid(other)' => '6017',
        'canid-coyote' => '6008',
        'canid-dingo' => '6011',
        'canid-doberman' => '6009',
        'canid-dog' => '6010',
        'canid-gsd' => '6012',
        'canid-husky' => '6014',
        'canid-jackal' => '6013',
        'canid-vulpine' => '6015',
        'canid-wolf' => '6016',
        'cephalopod' => '2001',
        'cervine' => '6018',
        'cetacean(other)' => '2004',
        'cetacean-dolphin' => '2002',
        'cetacean-whale' => '2003',
        'chakat' => '5003',
        'chocobo' => '5004',
        'citra' => '5005',
        'corvid' => '3001',
        'crocodile' => '7002',
        'crow' => '3002',
        'crux' => '5006',
        'daemon' => '5007',
        'digimon' => '5008',
        'dinosaur-sauropod' => '8001',
        'dinosaur-theropod' => '8002',
        'donkey' => '6019',
        'dracat' => '5009',
        'draenei' => '5010',
        'dragon(other)' => '4000',
        'duck' => '3003',
        'eagle' => '3004',
        'eastern' => '4001',
        'elf' => '5011',
        'exotic(other)' => '5000',
        'falcon' => '3005',
        'feline(other)' => '6030',
        'feline-cat' => '6020',
        'feline-cheetah' => '6021',
        'feline-cougar' => '6022',
        'feline-jaguar' => '6023',
        'feline-leopard' => '6024',
        'feline-lion' => '6025',
        'feline-lynx' => '6026',
        'feline-ocelot' => '6027',
        'feline-panther' => '6028',
        'feline-tiger' => '6029',
        'fish(general)' => '2005',
        'frog' => '1001',
        'gargoyle' => '5012',
        'gecko' => '7003',
        'giraffe' => '6031',
        'goose' => '3006',
        'gryphon' => '3007',
        'hawk' => '3008',
        'hedgehog' => '6032',
        'hippopotamus' => '6033',
        'horse' => '6034',
        'hydra' => '4002',
        'hyena' => '6035',
        'iguana' => '7004',
        'iksar' => '5013',
        'insect(other)' => '8003',
        'langurhali' => '5014',
        'lizard' => '7005',
        'llama' => '6036',
        'mammals(other)' => '6000',
        'mantid' => '8004',
        'marsupial(other)' => '6042',
        'marsupial-kangaroo' => '6038',
        'marsupial-koala' => '6039',
        'marsupial-opossum' => '6037',
        'marsupial-quoll' => '6040',
        'marsupial-wallaby' => '6041',
        'meerkat' => '6043',
        'mongoose' => '6044',
        'monster' => '5015',
        'moogle' => '5017',
        'mustelid(other)' => '6051',
        'mustelid-badger' => '6045',
        'mustelid-ferret' => '6046',
        'mustelid-mink' => '6048',
        'mustelid-otter' => '6047',
        'mustelid-weasel' => '6049',
        'mustelid-wolverine' => '6050',
        'naga' => '5016',
        'newt' => '1002',
        'orc' => '5018',
        'owl' => '3009',
        'panda' => '6052',
        'phoenix' => '3010',
        'pig/swine' => '6053',
        'pokemon' => '5019',
        'primate(other)' => '6058',
        'primate-gorilla' => '6054',
        'primate-human' => '6055',
        'primate-lemur' => '6056',
        'primate-monkey' => '6057',
        'rabbit' => '6059',
        'raccoon' => '6060',
        'rat' => '6061',
        'redpanda' => '6062',
        'reptilian(other)' => '7000',
        'rhinocerus' => '6063',
        'rodent(other)' => '6067',
        'rodent-beaver' => '6064',
        'rodent-mouse' => '6065',
        'rodent-rat' => '6066',
        'salamander' => '1003',
        'satyr' => '5020',
        'scorpion' => '8005',
        'seal' => '6068',
        'sergal' => '5021',
        'serpent' => '4003',
        'shark' => '2006',
        'skunk' => '6069',
        'snake' => '7006',
        'squirrel' => '6070',
        'swan' => '3011',
        'tanuki' => '5022',
        'turtle' => '7007',
        'unicorn' => '5023',
        'unspecified/any' => '1',
        'western' => '4004',
        'wyvern' => '4005',
        'xenomorph' => '5024',
        'zebra' => '6071'
    },
    'theme' => {
        '60s' => '207',
        '70s' => '206',
        '80s' => '205',
        '90s' => '204',
        'abstract' => '2',
        'all' => '1',
        'animalrelated(non-anthro)' => '3',
        'anime' => '4',
        'babyfur' => '101',
        'bondage' => '102',
        'classical' => '209',
        'comics' => '5',
        'digimon' => '103',
        'doodle' => '6',
        'fanart' => '7',
        'fantasy' => '8',
        'fatfurs' => '104',
        'fetishother' => '105',
        'fursuit' => '106',
        'gamemusic' => '210',
        'generalfurryart' => '100',
        'house' => '203',
        'human' => '9',
        'hyper' => '107',
        'industrial' => '214',
        'inflation' => '108',
        'macro/micro' => '109',
        'miscellaneous' => '14',
        'muscle' => '110',
        'mylittlepony/brony' => '111',
        'othermusic' => '200',
        'paw' => '112',
        'pokemon' => '113',
        'pop' => '212',
        'portraits' => '10',
        'pre-60s' => '208',
        'pregnancy' => '114',
        'rap' => '213',
        'rock' => '211',
        'scenery' => '11',
        'sonic' => '115',
        'stilllife' => '12',
        'techno' => '201',
        'trance' => '202',
        'transformation' => '116',
        'tutorials' => '13',
        'vore' => '117',
        'watersports' => '118'
    }
);

__END__

=head1 NAME

FurAffinity - interface with furaffinity.net

Still heavily under development!

=head1 VERSION

Version 0.02_01

=head1 CAVEATS

I'm gonna put these right here so that hopefully everybody reads them.

=head2 Use Responsibly

Don't do anything stupid with this. If you want to publish something based on
this, I<ask the site staff first>. That can save you, them and me a lot of
unecessary work and headaches.

Depending on what your script does, it might make someone mad and then all
your script's users get banned or the site layout gets nudged a bit to break
this entire module or in the worst case everyone using anything based on this
module gets banned.

Obviously, don't spam or create bogus users or do something else malicious.

But also keep the server load to acceptable levels. Don't run a bunch of
downloads in parallel. Don't poll the server at ridiculous intervals. Don't
give the user the ability to download half the site (a script that downloads
all submissions of every artist watched comes to mind).

=head2 Fragile

FurAffinity does not actually have an API, so this module may break randomly
without notice if something on FA changes, even something silly as minor layout
changes. There isn't really any way around that unless an API is implemented.

This module should notice if things aren't in the right place and then just
panic and die. In any case, it shouldn't be hard to fix, just report it as a
bug (or maybe even fix it yourself and make a pull request).

=head1 SYNOPSIS

    #!/usr/bin/perl
    use FurAffinity;

    # Make the module more verbose
    $FurAffinity::msg = 1;

    # Read your username from stdin, trim whitespace and newlines and construct
    print 'Username: ';
    my $fa = FurAffinity->new(<> =~ s/^\s*(\S*)\s*$/$1/r);

    # Read password from stdin, trim whitespace and newlines and try to log in
    print 'Password: ';
    $fa->log_in(<> =~ s/^\s*(\S*)\s*$/$1/r);

    # Check if the user is actually logged in
    if ($fa->check_logged_in) {

        # Fetch a minimal page that only has the top menu
        # with the user's name and their unread messages
        my %result = $fa->fetch_minimal;

        # Print their new messages to the console
        print "$result{user} has $result{messages}{total} messages.\n";
        for (qw(submissions journals comments favorites watches notes)) {
            print "$result{messages}{$_} $_\n";
        }

        # Every account is logged out automatically when it goes
        # out of scope, but you can also do it manually if desired
        $fa->log_out;
    }

If you want your passwords more hidden, use an encrypted file and pipe it into
your script or use a module like L<IO::Prompt>:

    use IO::Prompt;
    # Prompt for password and replace every entered character with an asterisk
    $fa->log_in(prompt 'Password: ', -e => '*');

=head1 DESCRIPTION

=head3 $fa->change_submission_file(%fields)

Changes the file of a submission.

%fields can be a hash or a hash reference with the following keys:

=over

=item id

The ID of the submission you want to modify.

=item file

The path to the file you want to replace the submission with.

=item rebuild_thumb (optional)

If true, the thumbnail of the submission will be replaced by a scaled-down
version of the newly uploaded picture. Note that this only applies to 100px
thumbnails, 150px and 200px thumbnails will always be regenerated because FA
is broken in that regard.

Only applies to artwork, other submission types don't have this field.

=back

Returns the submission ID on success. Throws a L</FormExcept> if the given
fields are malformed and a L</PageExcept> if the upload form doesn't look as
expected or if an error occurs and the result page isn't the submission's page
again.

=head3 $fa->submit_artwork(%fields)

Submits an image to FA.

%fields can be a hash or a hash reference with the following keys:

=over

=item file

The path to the image file you want to submit.

=item thumb (optional)

The path to the thumbnail for the submission. Note that this is only used for
100px thumbnails because FA is broken in that regard. If not provided, a
scaled-down version of the submission will be used instead.

=item title

The title for the submission, must have at least one non-whitespace character.

=item description

The descripton text for the submission, also must have at least one
non-whitespace character.

=item keywords (optional)

The searchable keywords for the submission, as a space-separated string. Can
only be up to 250 characters long.

=item rating

The rating for the submission, either ``general'', ``mature'' or ``adult''.

=item category (optional)

=item theme (optional)

=item species (optional)

=item gender (optional)

TODO

=back

Returns the submission's ID on success. Throws a L</FormExcept> if the given
fields are malformed and a L</PageExcept> if one of the submission steps
failed.

=head2 Internals

These subroutines are meant to be used internally, but I wouldn't go so far as
to call them private either. Just be aware that they could get broken in an
update more easily than the rest.

=head3 $fa->get(Str $url)

GETs the page at the given $url and returns a Mojo::DOM for the response. Throws
a L</RequestExcept> if the request fails.

This just dispatches to  L<WWW::Mechanize>'s get method, so you can also use any
parameters described there.

=head3 $fa->post(Str $url, HashRef $form_data)

POSTs $form_data to the given $url and returns a Mojo::DOM for the response.
Throws a L</RequestExcept> if the request fails.

This just dispatches to  L<WWW::Mechanize>'s post method, so you can also use
any parameters described there.

=head3 scrape_user(Mojo::DOM $dom)

Returns the username as it is written on the given page, for example
C<~askmeaboutloom> or C<@net-cat>. Returns C<undef> if there's no username on
the current page.

=head3 scrape_messages(Mojo::DOM $dom)

Returns a hash reference of how many messages of each type are written on the
current page and how many messages total there are. The hash has the following
keys:

=over

=item *

submissions

=item *

journals

=item *

comments

=item *

favorites

=item *

watches

=item *

notes

=item *

tickets

=item *

total

=back

=head3 scrape_submissions(Mojo::DOM $dom)

Returns a list reference of submissions on the current page. Works on all pages
with submissions thumbnails except on the front page. Each submission is a hash
reference with the following keys:

=over

=item type

The submission type, one of 'image', 'text', 'audio' or 'flash'.

=item id

The submission ID that appears in URLs regarding the submission.

=item artist

The name of the artist as it appears under the thumbnail. Note that this may
not be set because artist names don't appear in galleries and scraps.

=item title

The title of the submission as it appears under the thumbnail. Doesn't get set
on user pages because the titles don't appear there directly.

=back

=head2 Exceptions

=over

=item *

Except

=item *

ArgExcept

=item *

RequestExcept

=item *

PageExcept

=item *

FormExcept

=back

=head1 DEPENDENCIES

Aside from a moderately recent version of perl 5 you will need the following
modules:

=over

=item *

Mojo::DOM

=item *

WWW::Mechanize

=item *

Exception::Class

=back

All modules can be installed with a CPAN client using the following command
(replace C<cpan> if your client is called something else):

    cpan install Mojo::DOM WWW::Mechanize Exception::Class

=head1 AUTHORS

askmeaboutloom (askmeaboutloom@live.de)

=head1 COPYRIGHT AND LICENSE

This program is copyright (c) 2014 by its authors.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

=cut
