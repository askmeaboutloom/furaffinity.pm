package FurAffinity;
$VERSION = '0.02_01';
$VERSION = eval $VERSION;
use strict;
use warnings;
use Log::Message::Simple;
use Mojo::DOM;
use WWW::Mechanize;
use Exception::Class (
    Except        => {
        description => 'Base exception class for the FurAffinity module',
    },
    ArgExcept     => {
        isa         => 'Except',
        description => 'Incorrect or missing argument',
    },
    RequestExcept => {
        isa         => 'Except',
        description => 'Request failed',
        fields      => ['method'],
    },
    PageExcept    => {
        isa         => 'Except',
        description => "A page didn't look as expected",
    },
    FormExcept    => {
        isa         => 'Except',
        description => 'A form field was invalidated on the client side',
        fields      => ['field', 'check'],
    },
);

our $fa      = 'https://www.furaffinity.net';
our $charset = 'utf-8';
our $top     = 'table.block-menu-top > tr > td.header_bkg ';

our ($msg, $debug, %criteria);


sub new {
    my ($class, $name) = @_;
    Except->throw(error => 'Missing name in constructor') if !defined $name;
    Except->throw(error => 'Empty name in constructor'  ) if $name !~ /\S/;
    bless {
        name      => $name,
        mech      => WWW::Mechanize->new,
        logged_in => 0,
    } => $class;
}

sub DESTROY {
    my ($self) = @_;
    $self->log_out if $self->logged_in;
}

sub name      { $_[0]->{name} }
sub mech      { $_[0]->{mech} }
sub logged_in {
    my ($self, $arg) = @_;
    $self->{logged_in} = $arg if defined $arg;
    return $self->{logged_in};
}


sub dom { Mojo::DOM->new($_[0]->mech->content(charset => $charset)) }

sub get {
    my $self = shift;
    my $mech = $self->mech;
    debug "get @_", $debug;
    $mech->get(@_);
    return $self->dom if $mech->success;
    RequestExcept->throw(method => 'GET', error => $mech->res->status_line);
};

sub post {
    my $self = shift;
    my $mech = $self->mech;
    debug "post @_", $debug;
    $mech->post(@_);
    return $self->dom if $mech->success;
    RequestExcept->throw(method => 'POST', error => $mech->res->status_line);
};


sub log_in {
    my ($self, $pass) = @_;
    msg 'Logging in ' . $self->name, $msg;
    $self->get("$fa/login/");
    eval {
        $self->mech->submit_form(
            fields => {name => $self->name, pass => $pass}
        );
    };
    PageExcept->throw(error => $@) if $@;
    return $self;
}

sub log_out {
    my ($self) = @_;
    msg 'Logging out ' . $self->name, $msg;
    $self->get("$fa/logout/");
    return $self;
}

sub check_logged_in {
    my ($self, $dom) = @_;
    msg 'Checking login status of ' . $self->name, $msg;
    $dom //= $self->get($fa);

    return $self->logged_in(1) if defined $self->scrape_user($dom);
    return $self->logged_in(0) if $dom->at("$top> .dropdown-left > .noblock");

    PageExcept->throw(error => "Can't tell if " . $self->name
                             . ' is logged in or not.');
}


our %field_checkers = (
    max      => sub { length $_[0] <= $_[1]        },
    min      => sub { length $_[0] >= $_[1]        },
    regex    => sub { $_[0] =~ $_[1]               },
    inhash   => sub { exists $_[1]->{$_[0]}        },
    exists   => sub { $_[1] ? -e $_[0] : !-e $_[0] },
    optional => sub { 1                            },
);

sub check_fields {
    my ($fields, $restrictions) = @_;
    while (my ($name, $restrict) = each %$restrictions) {
        my $field = $fields->{$name};
        next if $restrict->{optional} && !defined $field;
        while (my ($k, $v) = each %$restrict) {
            if (!$field_checkers{$k}->($field, $v)) {
                FormExcept->throw(
                    error => "$name does not pass $k => $v with $field",
                    field => [$name => $field],
                    check => [$k    => $v    ],
                );
            }
        }
    }
}


sub scrape_user {
    my ($self, $dom) = @_;
    my $sel = $dom->at('#my-username');
    return $sel->text . '' if $sel;
    return undef;
}

sub scrape_messages {
    my ($self, $dom) = @_;

    # No messages
    if ($dom->at("$top> .dropdown-left > .noblock > a:not([href]) > b")) {
        return {map { $_ => 0 } qw(total submissions journals comments
                                   favorites watches notes tickets)};
    }

    my $sel   = $dom->at("$top> .dropdown-left > .noblock");
    my $sel_s = $sel->at('a[href="/msg/submissions/"]'  );
    my $sel_o = $sel->at('a[href="/msg/others/"]'       );
    my $sel_n = $sel->at('a[href="/msg/pms/"]'          );
    my $sel_t = $sel->at('a[href="/msg/troubletickets"]');
    return undef unless $sel && $sel_s && $sel_o && $sel_n && $sel_t;

    my $total = 0;
    my %messages;
    # Match number of each message type and on success put it into %messages.
    # On failure (no messages of that type), put 0 into it. Whatever was put
    # into the hash will be added to the $total amount of messages.
    $total += $messages{submissions} = $sel_s->text =~ /(\d+)/  ? int $1 : 0;
    $total += $messages{comments   } = $sel_o->text =~ /(\d+)C/ ? int $1 : 0;
    $total += $messages{journals   } = $sel_o->text =~ /(\d+)J/ ? int $1 : 0;
    $total += $messages{favorites  } = $sel_o->text =~ /(\d+)F/ ? int $1 : 0;
    $total += $messages{watches    } = $sel_o->text =~ /(\d+)W/ ? int $1 : 0;
    $total += $messages{notes      } = $sel_n->text =~ /(\d+)/  ? int $1 : 0;
    # TODO: Does this work? I never had any trouble tickets.
    $total += $messages{tickets    } = $sel_t->text =~ /(\d+)/  ? int $1 : 0;
              $messages{total      } = $total;

    return \%messages;
}

sub scrape_submissions {
    my ($self, $dom) = @_;
    my @submissions;

    for my $type (qw(image text audio flash)) { # are there more types maybe?
        for my $sel ($dom->at(".t-$type")->each) {
            my $id     = $sel->at('u > s > a')->attr('href')
                                                 =~ m{^/view/(.+)/$} && $1;
            my $artist = $sel->at('small > a')->attr('href')
                                                 =~ m{^/user/(.+)/$} && $1;
            my $thumb  = 'https:' . $sel->at('img')->attr('src');
            $thumb =~ s/@\d+?-/\@100-/;
            push @submissions, {
                type   => $type,
                id     => $id,
                artist => $artist,
                title  => $sel->at('span')->text . '',
                thumb  => $thumb,
            };
        }
    }

    return \@submissions;
}

sub scrape_submission {
    my ($self, $dom) = @_;
    my %submission;

    # the page title has both the submission title and the artist
    my $title = $dom->at('title')->first->text;
    if ($title =~ /^\s*(.*)\s*by\s*(.*?)\s*-- Fur Affinity \[dot\] net\s*$/) {
        $submission{title } = $1;
        $submission{artist} = $2;
    }

    for my $sel ($dom->at('.alt1.actions b > a')->each) {
        my $href = $sel->attr('href');

        if      ($href =~ m{^//d\.facdn\.net/(.*)$}) {
           $submission{file      } = "https://d.facdn.net/$1";

        } elsif ($href =~ m{^/full/(\d+)/$}
              || $href =~ m{^/view/(\d+)/$}) {
            $submission{id        } = $1;

        } elsif ($href =~ m{^/gallery/(.*)/$}) {
            $submission{artist_url} = $1;
        }
    }

    my $desc = $dom->at('td[valign="top"][align="left"]'
                        . '[width="70%"][class="alt1"]')->content;
    $submission{description} = "$desc";

    my $date = $dom->at('td[valign="top"][align="left"][class="alt1"]'
                        . '> .popup_date');
    # Depending on the user's setting, the absolute date is in the text and the
    # fuzzy date is in the title or vice-versa. We want the absolute one, so we
    # match against something like "August 15th, 2014 12:34 PM".
    # regex breakdown:  month    day  th,     year   hour:minute   AM/PM
    if ($date->text =~ / \S+ \s+ \d+ \S+, \s+ \d+ \s+ \d+:\d+   \s+ \S+ /x) {
        $submission{date} = $date->text . '';
    } else {
        $submission{date} = $date->attr('title') . '';
    }

    $submission{keywords} = [];
    for my $sel ($dom->at('#keywords > a')->each) {
        push @{$submission{keywords}}, $sel->text;
    }

    # TODO comments
    # TODO maybe other submission info, but eh, scrape_info already does that

    return \%submission;
}

our %id_to_rating = qw(0 general 1 adult 2 mature);

sub scrape_info {
    my ($self, $dom) = @_;
    my  %info;

    for (['cat', 'category'], ['atype', 'theme'], 'species', 'gender') {
        my ($name, $key) = ref $_ ? @$_ : ($_, $_);
        my $text = $dom->at(qq(select[name="$name"] option[selected]))->text;
        $info{$key} = "$text";
    }

    $info{scraps} = $dom->at('input[name="scrap"][checked]')->children->size;

    my $rid = $dom->at('input[name="rating"][checked]')->val;
    $info{rating} = $id_to_rating{$rid};

    my $title = $dom->at('input[name="title"]')->val;
    $info{title} = "$title";

    my $description = $dom->at('textarea[name="message"]')->text;
    $info{description} = "$description";

    my @keywords = split /\s+/, $dom->at('textarea[name="keywords"]')->text;
    $info{keywords} = \@keywords;

    return \%info;
}

our %scrapers = (
    user        => 'scrape_user',
    messages    => 'scrape_messages',
    submissions => 'scrape_submissions',
    submission  => 'scrape_submission',
    info        => 'scrape_info',
);

sub scrape {
    my $self = shift;
    my %args = ref $_[0] eq 'HASH' ? %{$_[0]} : @_;

    my $list = ref $args{scrape} eq 'ARRAY' && $args{scrape}
        or ArgExcept->throw(error => 'Missing list of scrapers');

    my $pg  = $args{page};
    my $dom = defined $pg ? ref $pg ? $pg : $self->get($pg) : $self->dom;

    my %result;
    for my $key (@$list) {
        my $scraper = $scrapers{$key}
            or ArgExcept->throw(error => "``$key'' is not a scraper");
        $result{$key} = $self->$scraper($dom);
    }
    return \%result;
}


sub fetch_messages {
    my ($self, $page) = @_;
    return $self->scrape(
        page   => $page // $self->get($fa),
        scrape => ['messages'],
    )->{messages};
}

sub fetch_submissions {
    my $self = shift;
    my %args = (
        from   => undef,
        number => 1,
        user   => $self->name,
        ref $_[0] eq 'HASH' ? %{$_[0]} : @_,
    );

    $args{from} =~ /^(gallery|scraps|favorites)$/
        or ArgExcept->throw(error => "Can't get submissions from ``$args{from}"
                                   . "''. Valid values are ``gallery'', "
                                   . "``scraps'' and ``favorites''.");

    return $self->scrape(
        page   => "$fa/$args{from}/$args{user}/$args{number}/",
        scrape => ['submissions'],
    )->{submissions};
}

sub fetch_submission {
    my $self = shift;
    my %args = ref $_[0] eq 'HASH' ? %{$_[0]} : @_;

    $args{id}=~ /^\d+$/
        or ArgExcept->throw(error => "ID must be an unsigned number.");

    my $submission = $self->scrape(
        page   => "$fa/view/$args{id}/",
        scrape => [qw(submission)],
    );

    if ($submission->{submission}{artist} eq $self->name) {
        return {%$submission, %{$self->scrape(
            page   => "$fa/controls/submissions/changeinfo/$args{id}/",
            scrape => [qw(info)],
        )}};
    } else {
        return $submission;
    }
}


sub change_info {
    my $self = shift;
    my $mech = $self->mech;
    my %fields = ref $_[0] eq 'HASH' ? %{$_[0]} : @_;

    check_fields(\%fields, {
        id          => {regex => qw/^\d+$/              },
        title       => {regex  => qr/\S/,  optional => 1},
        description => {regex  => qr/\S/,  optional => 1},
        keywords    => {max    => 250,     optional => 1},
        # TODO other fields
    });

    my %form;
    $form{title   } = $fields{title      } if exists $fields{title      };
    $form{message } = $fields{description} if exists $fields{description};
    $form{keywords} = $fields{keywords   } if exists $fields{keywords   };

    $self->get("$fa/controls/submissions/changeinfo/$fields{id}/")
        unless $fields{no_load};
    eval {
        $mech->submit_form(
            form_name => 'MsgForm',
            fields    => \%form,
        );
    };
    PageExcept->throw(error => $@) if $@;

    $mech->uri =~ m{/view/$fields{id}/} or PageExcept->throw(
        error => 'Unexpected submission result URL: ' . $mech->uri,
    );
}

sub change_submission_file {
    my $self   = shift;
    my $mech   = $self->mech;
    my %fields = (
        rebuild_thumb => 0,
        ref $_[0] eq 'HASH' ? %{$_[0]} : @_,
    );

    check_fields(\%fields, {
        id            => {regex  => qw/^\d+$/},
        file          => {exists => 1        },
    });

    $self->get("$fa/controls/submissions/changesubmission/$fields{id}/");
    eval {
        $mech->submit_form(
            form_name => 'uploadform',
            fields    => {
                'newsubmission'     => $fields{file},
                'rebuild-thumbnail' => $fields{rebuild_thumb} ? 1 : 0,
            },
        );
    };
    PageExcept->throw(error => $@) if $@;

    $mech->uri =~ m{/view/(\d+)/} or PageExcept->throw(
        error => 'Unexpected submission result URL: ' . $mech->uri,
    );
    return int $1;
}


our %rating_to_id = reverse %id_to_rating;

sub submit_artwork {
    my $self  = shift;
    my $mech  = $self->mech;
    my %fields = (
        keywords => '',
        ref $_[0] eq 'HASH' ? %{$_[0]} : @_,
    );
    for my $key (keys %criteria) {
        next if not defined $fields{$key};
        $fields{$key} =~ s/\s+//g;
        $fields{$key} =  lc $fields{$key};
    }
    msg 'Submitting artwork for ' . $self->name, $msg;

    check_fields(\%fields, {
        file        => {exists => 1                  },
        thumb       => {exists => 1, optional => 1   },
        title       => {regex  => qr/\S/             },
        description => {regex  => qr/\S/             },
        keywords    => {max    => 250                },
        rating      => {inhash => \%rating_to_id     },
        category    => {inhash => $criteria{category}},
        theme       => {inhash => $criteria{theme   }},
        species     => {inhash => $criteria{species }},
        gender      => {inhash => $criteria{gender  }},
    });

    msg 'Starting submission process...', $msg;
    $self->get("$fa/submit/");
    eval {
        $mech->submit_form(
            form_name => 'myform',
            fields    => {submission_type => 'submission'},
        );
    };
    PageExcept->throw(error => $@) if $@;

    msg 'Uploading...', $msg;
    eval {
        $mech->submit_form(
            form_name => 'myform',
            fields    => {
                submission => $fields{file},
                defined $fields{thumb} ? (thumbnail => $fields{thumb}) : (),
            },
        );
    };
    PageExcept->throw(error => $@) if $@;

    msg 'Finalizing...', $msg;
    eval {
        $mech->submit_form(
            form_name => 'myform',
            fields    => {
                title    => $fields{title},
                message  => $fields{description},
                keywords => $fields{keywords},
                rating   => $fields{rating},
                scrap    => $fields{scraps} ? 1 : 0,
                cat      => $criteria{category}{$fields{category}} // 1,
                atype    => $criteria{theme   }{$fields{theme   }} // 1,
                species  => $criteria{species }{$fields{species }} // 1,
                gender   => $criteria{gender  }{$fields{gender  }} // 1,
            },
        );
    };
    PageExcept->throw(error => $@) if $@;

    $mech->uri =~ m{/view/(\d+)/} or PageExcept->throw(
        error => 'Unexpected submission result URL: ' . $mech->uri,
    );
    return int $1;
}

sub submit_note {
    my $self   = shift;
    my %fields = (
        subject => '',
        ref $_[0] eq 'HASH' ? %{$_[0]} : @_,
    );

    check_fields(\%fields, {
        to      => {max => 150, regex => qr/\S/},
        subject => {max => 150                 },
        message => {            regex => qr/\S/},
    });

    msg 'Sending message from ' . $self->name . " to $fields{to}", $msg;
    $self->get("$fa/msg/compose/");
    eval {
        $self->mech->submit_form(
            form_name => 'MsgForm',
            fields    => \%fields,
        );
    };
    PageExcept->throw(error => $@) if $@;
    # TODO: check if that worked
    return $self;
}


do 'criteria.pl';
