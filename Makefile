all: dist doc

dist: dist/FurAffinity.pm

doc: doc/FurAffinity.html

clean:
	rm -rf criteria.pl dist

test:
	prove


criteria.pl: html/criteria.html scripts/extract_criteria
	perl scripts/extract_criteria < $< > $@

FurAffinity.pm: criteria.pl

dist/FurAffinity.pm: criteria.pl FurAffinity.pm \
                     doc/FurAffinity.pod scripts/mkdist
	mkdir -p dist
	perl scripts/mkdist FurAffinity.pm criteria.pl doc/FurAffinity.pod > $@

doc/FurAffinity.html: doc/FurAffinity.pod
	perl -MPod::Simple::HTML -e \
    '$$ps=Pod::Simple::HTML->new; $$ps->index(1); $$ps->parse_from_file;' \
    < doc/FurAffinity.pod > doc/FurAffinity.html

install:
	cpan install Exception::Class Mojo::DOM Test::Most WWW::Mechanize

.PHONY: all clean dist doc install test
